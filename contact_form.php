<!doctype html>
<html class="no-js" lang="en"> 
<head>
<?php include "include/header.php"; ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>

#loading-img{
display:none;
}

.response_msg{
margin-top:10px;
font-size:13px;
background:#E5D669;
color:#ffffff;
width:250px;
padding:3px;
display:none;
}

</style>
        
</head>
<body>
        <div class="wrapper home-one single-product-page">
            <!-- HEADER AREA START -->
            <?php include 'include/navbar.php'; ?>
            
            <!-- HRADER AREA END -->
            <!-- Breadcrumbs -->
            <div class="breadcrumbs-container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav class="woocommerce-breadcrumb">
                                <a href="index.php">Home</a>
                                <span class="separator">/</span> Cart
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcrumbs End -->
            <!-- Page title -->
            <div class="entry-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="entry-title">Cart</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page title end -->
            <!-- cart page content -->
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <form name="contact-form" action="get_response.php" method="post" id="contact-form">
                            <h3>Billing Address</h3>
                            <div class="form-group">
                                <label for="Name"><i class="fa fa-user"></i>Name</label>
                                <input type="text" id="fname" name="your_name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1"><i class="fa fa-envelope"></i> Email address</label>
                                <input type="email" class="form-control" name="your_email" placeholder="@gmail.com" required>
                            </div>
                            <div class="form-group">
                                <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                                <input type="text" id="adr" name="address" placeholder="House name, Street">
                            </div>
                            <div class="form-group">
                                <label for="city"><i class="fa fa-institution"></i> City</label>
                                <input type="text" id="city" name="city" placeholder="Enter current city">
                            </div>
                            <button type="submit" class="btn" name="submit" value="Submit" id="submit_form">submit</button>
                        </form>
                        <div class="response_msg"></div>
                    </div>
                </div>

            </div>
            <!-- cart page content end -->
            <?php include 'include/footer.php'; ?>













        </div>
        <!-- Body main wrapper end -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        

        <!-- including polyfills for older browsers -->
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    <script src="js/shop.js"></script>
    <script src="js/index.js"></script>

    <!-- include script for google pay with onload handler -->
    <script async
      src="https://pay.google.com/gp/p/js/pay.js"
      onload="onGooglePayLoaded()">
    </script>
    </body>
</html>
